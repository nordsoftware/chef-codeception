#
# Cookbook Name:: codeception
# Recipe:: default
#
# Copyright 2014, NordSoftware Oy
#

if node[:codeception][:install_for_yii2]
	node[:codeception][:install_globally] = false
	link "#{node[:codeception][:prefix]}/codecept" do
		to "/home/vagrant/app/vendor/codeception/codeception/codecept"
	end

end

if node[:codeception][:install_globally]
	remote_file "#{node[:codeception][:prefix]}/codecept" do
		source "#{node[:codeception][:url]}"
		owner "root"
		group "root"
		mode 0755
		action :create_if_missing
	end
end
