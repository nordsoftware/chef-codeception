#
# Cookbook Name:: composer
# Attributes:: default
#
# Copyright 2012-2013, Escape Studios
#

default[:codeception][:install_globally] = true
default[:codeception][:install_for_yii2] = false
default[:codeception][:prefix] = "/usr/local/bin"
default[:codeception][:url] = "https://github.com/Codeception/codeception.github.com/raw/master/codecept.phar"
default[:codeception][:install_dir] = "."